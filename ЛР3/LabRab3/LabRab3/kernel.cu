
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <cstdio>
#include <time.h>
#include <stdlib.h>
#include <locale.h>  

#define BLOCK_SIZE 64        // ���-�� ������� � �����
#define N 4096        // ����������� ������� NxN (���������� � ���� �������)

__global__ void mult ( int *a, int *b, int *c, int n)
{
    int bx = blockIdx.x;     
    int by = blockIdx.y;
    int tx = threadIdx.x;       
    int ty = threadIdx.y;
    int res = 0;           
    int ia = n * BLOCK_SIZE * by + n * ty;   // a [i][0]
    int ib = BLOCK_SIZE * bx + tx;
    
                           
    for ( int k = 0; k < n; k++ )
        res += a[ia + k] * 5 - b[ib + k*n] * 8 + a[ia + k] * b[ib + k*n]; // A*5 - B*8 + A*B 
            
      //��������� ������ ��� ������ � ���������� ����������                      
    int ic = n * BLOCK_SIZE * by + BLOCK_SIZE * bx;
    
    c [ic + n * ty + tx] = res;
}

int main ( int argc, char *  argv [] )
{
	setlocale(LC_ALL,"RUS"); //����� ������� ������������ �������

	//���������� �����
	int *a, *b, *c;

	//�������� ������ �� �����
	a = (int*)malloc(N*N*sizeof(int));
	b = (int*)malloc(N*N*sizeof(int));
	c = (int*)malloc(N*N*sizeof(int));
    int bytesSize = N * N * sizeof ( int );

                 
   
    
    for ( int i = 0; i < N*N; i++ )
	{
		int r = rand() % 5; // ����� �� 0 �� 4
        a[i] = r;
		r = rand() % 5;
        b[i] = r;
	}
        
    // ���������� ���
    int *a_dev, *b_dev, *c_dev;
    
	//�������� ������ �� ���
    cudaMalloc ( (void**)&a_dev, bytesSize );
    cudaMalloc ( (void**)&b_dev, bytesSize );
    cudaMalloc ( (void**)&c_dev, bytesSize );

                    
    dim3 threads ( BLOCK_SIZE, BLOCK_SIZE ); //������ ���-�� �������
    dim3 blocks  ( N / threads.x, N / threads.y); //������

    //�������� �����
    cudaEvent_t start, stop;
    float gpuTime = 0.0f;

    cudaEventCreate ( &start );
    cudaEventCreate ( &stop );
    
	int times = 0;
	float timeSum = 0;
    
	while(times < 100) // ��������� 100 ��� ��� ��������
	{
		cudaEventRecord ( start, 0 ); //
		//�������� ���������� � ���
		cudaMemcpy      ( a_dev, a, bytesSize, cudaMemcpyHostToDevice );
		cudaMemcpy      ( b_dev, b, bytesSize, cudaMemcpyHostToDevice );
		//��������� ������� ����������
		mult<<<blocks, threads>>> ( a_dev, b_dev, c_dev, N);
    
		cudaMemcpy      ( c, c_dev, bytesSize, cudaMemcpyDeviceToHost );
		cudaEventRecord ( stop, 0 );

		cudaEventSynchronize ( stop );
		cudaEventElapsedTime ( &gpuTime, start, stop );

    
		printf("%i: %.2f ��\n", times+1, gpuTime);
		timeSum += gpuTime;
		times++;
	}

	printf("������� �����: %.2f ��\n", timeSum / 100 );
	std::getchar();
    

	//������� ������
    cudaEventDestroy (start);
    cudaEventDestroy (stop);
    cudaFree         (a_dev);
    cudaFree         (b_dev);
    cudaFree         (c_dev);

    free(a);
    free(b);
    free(c);

    return 0;
}