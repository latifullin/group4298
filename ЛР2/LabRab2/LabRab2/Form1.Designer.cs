﻿namespace LabRab2
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pict = new System.Windows.Forms.PictureBox();
            this.startBtn = new System.Windows.Forms.Button();
            this.stopBtn = new System.Windows.Forms.Button();
            this.points = new System.Windows.Forms.Label();
            this.resultBox = new System.Windows.Forms.ListBox();
            this.resLabel = new System.Windows.Forms.Label();
            this.startParal = new System.Windows.Forms.Button();
            this.resLabelPar = new System.Windows.Forms.Label();
            this.clearBtn = new System.Windows.Forms.Button();
            this.paintBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pict)).BeginInit();
            this.SuspendLayout();
            // 
            // pict
            // 
            this.pict.BackColor = System.Drawing.SystemColors.Control;
            this.pict.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pict.Location = new System.Drawing.Point(6, 6);
            this.pict.Name = "pict";
            this.pict.Size = new System.Drawing.Size(600, 600);
            this.pict.TabIndex = 0;
            this.pict.TabStop = false;
            this.pict.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pict_MouseClick);
            // 
            // startBtn
            // 
            this.startBtn.Location = new System.Drawing.Point(616, 471);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(103, 23);
            this.startBtn.TabIndex = 1;
            this.startBtn.Text = "Старт (послед)";
            this.startBtn.UseVisualStyleBackColor = true;
            this.startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // stopBtn
            // 
            this.stopBtn.Location = new System.Drawing.Point(616, 547);
            this.stopBtn.Name = "stopBtn";
            this.stopBtn.Size = new System.Drawing.Size(75, 23);
            this.stopBtn.TabIndex = 2;
            this.stopBtn.Text = "Стоп";
            this.stopBtn.UseVisualStyleBackColor = true;
            this.stopBtn.Click += new System.EventHandler(this.stopBtn_Click);
            // 
            // points
            // 
            this.points.AutoSize = true;
            this.points.Location = new System.Drawing.Point(613, 593);
            this.points.Name = "points";
            this.points.Size = new System.Drawing.Size(63, 13);
            this.points.TabIndex = 3;
            this.points.Text = "Отмечено: ";
            // 
            // resultBox
            // 
            this.resultBox.FormattingEnabled = true;
            this.resultBox.Location = new System.Drawing.Point(612, 6);
            this.resultBox.Name = "resultBox";
            this.resultBox.Size = new System.Drawing.Size(276, 459);
            this.resultBox.TabIndex = 4;
            // 
            // resLabel
            // 
            this.resLabel.AutoSize = true;
            this.resLabel.Location = new System.Drawing.Point(725, 476);
            this.resLabel.Name = "resLabel";
            this.resLabel.Size = new System.Drawing.Size(85, 13);
            this.resLabel.TabIndex = 5;
            this.resLabel.Text = "Результат (мс):";
            // 
            // startParal
            // 
            this.startParal.Location = new System.Drawing.Point(616, 501);
            this.startParal.Name = "startParal";
            this.startParal.Size = new System.Drawing.Size(103, 23);
            this.startParal.TabIndex = 6;
            this.startParal.Text = "Старт (парал)";
            this.startParal.UseVisualStyleBackColor = true;
            this.startParal.Click += new System.EventHandler(this.startParal_Click);
            // 
            // resLabelPar
            // 
            this.resLabelPar.AutoSize = true;
            this.resLabelPar.Location = new System.Drawing.Point(725, 506);
            this.resLabelPar.Name = "resLabelPar";
            this.resLabelPar.Size = new System.Drawing.Size(85, 13);
            this.resLabelPar.TabIndex = 7;
            this.resLabelPar.Text = "Результат (мс):";
            // 
            // clearBtn
            // 
            this.clearBtn.Location = new System.Drawing.Point(802, 547);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(75, 23);
            this.clearBtn.TabIndex = 8;
            this.clearBtn.Text = "Очистить";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.clearBtn_Click);
            // 
            // paintBtn
            // 
            this.paintBtn.Location = new System.Drawing.Point(721, 547);
            this.paintBtn.Name = "paintBtn";
            this.paintBtn.Size = new System.Drawing.Size(75, 23);
            this.paintBtn.TabIndex = 9;
            this.paintBtn.Text = "Показать";
            this.paintBtn.UseVisualStyleBackColor = true;
            this.paintBtn.Click += new System.EventHandler(this.paintBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 613);
            this.Controls.Add(this.paintBtn);
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.resLabelPar);
            this.Controls.Add(this.startParal);
            this.Controls.Add(this.resLabel);
            this.Controls.Add(this.resultBox);
            this.Controls.Add(this.points);
            this.Controls.Add(this.stopBtn);
            this.Controls.Add(this.startBtn);
            this.Controls.Add(this.pict);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pict)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pict;
        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.Button stopBtn;
        private System.Windows.Forms.Label points;
        private System.Windows.Forms.ListBox resultBox;
        private System.Windows.Forms.Label resLabel;
        private System.Windows.Forms.Button startParal;
        private System.Windows.Forms.Label resLabelPar;
        private System.Windows.Forms.Button clearBtn;
        private System.Windows.Forms.Button paintBtn;
    }
}

