﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace LabRab2
{
    public partial class Form1 : Form
    {
        int size;
        int count;
        int[,] table;
         Graphics g;
        Pen p;
        DateTime start, stop;
        TimeSpan ts;
        int epoch;
        bool cancel;
        List<double> results;
        List<double> resultsPar;

        CancellationTokenSource cancelTokenSource; //для создания маркера отмены
        CancellationToken token; //сам токен отмены

        public Form1()
        {
            InitializeComponent();
            System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
            cancelTokenSource = new CancellationTokenSource();
            token = cancelTokenSource.Token;
            pict.Image = (Image)new Bitmap(pict.Width, pict.Height);
            epoch = 0;
            size = 40;
            count = 0;
            results = new List<double>();
            resultsPar = new List<double>();
            table = new int[size, size];
            drawGrid();
        }

        private void drawGrid() 
        {
            count = 0;
          //  points.Text = "Отмечено: ";
            pict.Image = null;
            pict.Image = (Image)new Bitmap(pict.Width, pict.Height);
            g = Graphics.FromImage(pict.Image);
            p = new Pen(Color.Black);
            for (int i = 0; i < size; i++)
            {
                g.DrawLine(p, new Point((pict.Width / size * (i + 1)), 0), new Point((pict.Width / size * (i + 1)), pict.Height));
                g.DrawLine(p, new Point(0, (pict.Height / size * (i + 1))), new Point(pict.Width, (pict.Height / size * (i + 1))));
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void pict_MouseClick(object sender, MouseEventArgs e)
        {
            int X = pict.Width / size;
            int Y = pict.Height / size;

            for (int i = 0; i < size; i++)
            {
                if (e.X <= (X * (i+1)))
                {
                    for (int j = 0; j < size; j++)
                    {
                        if (e.Y <= (Y * (j+1)))
                        {
                            if (table[i, j] != 1)
                            {
                                Graphics a = Graphics.FromImage(pict.Image);
                                a.FillRectangle(Brushes.Red, (X * i), (Y * j), X, Y);
                                this.Refresh();
                                count++;
                                table[i, j] = 1;
                                points.Text = "Отмечено: " + count;
                                
                            } 
                            break;
                        }
                    }
                    break;
                }
            }
        }

        private int[,] posled()
        {             
            int[,] newTable = new int[size,size];
            for(int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    newTable[i, j] = getState(calcPotential(i, j));
            return newTable;
        }

        private int calcPotential(int i, int j)
        {
            int p = 0;
            for (int x = i - 1; x <= i + 1; x++) 
                for (int y = j - 1; y <= j + 1; y++) 
                { 
                    if (x < 0 || y < 0 || x >= size|| y >= size || (x == i && y == j)) 
                        continue; 
                    if (table[x, y] == 1)  
                        p++; 
                } 
            return p;
        }

        private int[,] paral()
        {
            int[,] newTable = new int[size, size];
            try
            {
                Parallel.For(0, size, new ParallelOptions { CancellationToken = token }, (i) =>
                {//for (int i = 0; i < size; i++)
                    for (int j = 0; j < size; j++)
                        newTable[i, j] = getState(calcPotential(i, j));
                  //  Thread.Sleep(10);
                });
            }
            catch (OperationCanceledException ex)
            {
                resLabelPar.Text = "Прервано";
            }
       

            return newTable;
        }



        private int getState(int p)
        {
            if (p == 3)
                return 1;
            else if (p == 2)
                return 1;
            else
                return 0;
        }



        private void fillGrid()
        {
            int X = pict.Width / size;
            int Y = pict.Height / size;

            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                {
                    if (table[i, j] == 1)
                    {
                        Graphics a = Graphics.FromImage(pict.Image);
                        a.FillRectangle(Brushes.Red, (X * i), (Y * j), X, Y);
                        this.Refresh();
                        count++;
                    }
                }

            points.Text = "Отмечено: " + count;
        }

        private void stopBtn_Click(object sender, EventArgs e)
        {
            cancelTokenSource.Cancel(); 
        }

        private void startBtn_Click(object sender, EventArgs e)
        {
            epoch = 0;
            results.Clear();
            resultBox.Items.Clear();
            int i = 0;

            while (i < 500)
            {
                start = DateTime.Now;
                table = posled();
                stop = DateTime.Now;


                ts = stop - start;
                epoch++;
                resultBox.Items.Add("Эпоха: " + epoch + " Время: " + ts.TotalMilliseconds);
                results.Add(ts.TotalMilliseconds);
                i++;
            }

            drawGrid();
            fillGrid();
            List<double> temp = results.ToList();
            if (results.Count > 10)
                temp.RemoveRange(0, 5); //первые 5 эпох пропустим
            resLabel.Text = "Результат (мс): " + temp.Sum() / epoch;
        }

        private void startParal_Click(object sender, EventArgs e)
        {
            epoch = 0;
            resultsPar.Clear();
            resultBox.Items.Clear();
            Thread t = new Thread(new ThreadStart(() =>
            {
            while (epoch < 500)
            {
                if (token.IsCancellationRequested)
                    break;
                start = DateTime.Now;
                table = paral();            
                stop = DateTime.Now;
                ts = stop - start;
                epoch++;
                resultBox.Items.Add("Эпоха: " + epoch + " Время: " + ts.TotalMilliseconds);
                resultsPar.Add(ts.TotalMilliseconds);
            }
            }));
            t.Start();
       //     t.Join();

            //  for(int i = 0 ; i < resultsPar.Count; i++)
            //     resultBox.Items.Add("Эпоха: " + i + " Время: " + resultsPar[i]);
     
        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            pict.Image = (Image)new Bitmap(pict.Width, pict.Height);
            epoch = 0;
            size = 40;
            count = 0;
            results = new List<double>();
            resultsPar = new List<double>();
            table = new int[size, size];
            drawGrid();
            epoch = 0;

            resultBox.Items.Clear();
        }

        private void paintBtn_Click(object sender, EventArgs e)
        {
            if (resultsPar.Count <= 0)
            {
                MessageBox.Show("Сначала запустите алгоритм");
                return;
            }
            
            drawGrid();
            fillGrid();

            List<double> temp = resultsPar.ToList();
            if (resultsPar.Count > 10)
                temp.RemoveRange(0, 5); //первые 5 эпох пропустим

            resLabelPar.Text = "Результат (мс): " + temp.Sum() / epoch;
        }
    }
}
