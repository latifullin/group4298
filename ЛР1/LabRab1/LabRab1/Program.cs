﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace LabRab1
{
    class Data
    {
        public int K;
        public int start;
        public int stop;
        public double[] a;
        public int i;
        public int[] indexes;

        public Data(int K, int start, int stop, double[] a, int i)
        {
            this.K = K;
            this.start = start;
            this.stop = stop;
            this.a = a;
            this.i = i;
        }
    }

    class Program
    {


        static void Main(string[] args)
        {
            Console.WriteLine("Число потоков: " + System.Environment.ProcessorCount);
            char menu;
            int N, M, K;
            double[] a;


            Data[] data;
            DateTime start, stop;
            TimeSpan ts;
            List<double> res1 = new List<double>();
            List<double> res2 = new List<double>();
            Random rnd = new Random();
            Console.Write("Введите размер массива N:");
            Int32.TryParse(Console.ReadLine(), out N);
            Console.Write("Введите число потоков M:");
            Int32.TryParse(Console.ReadLine(), out M);
            Console.Write("Введите сложность K:");
            Int32.TryParse(Console.ReadLine(), out K);
            a = new double[N];
            for (int i = 0; i < N; i++)
                a[i] = rnd.Next(0, 100);

            do
            {


                Console.WriteLine("\n1 - Ввести N \n2 - Ввести M \n3 - Ввести K \n4 - Последовательная обработка \n5 - Многопоточная обработка \n6 - Многопоточная круговая \n7 - Вывести результаты\nq - Выход");
                Console.Write("\n\nВыберите пункт меню:");
                Char.TryParse(Console.ReadLine(), out menu);
                switch (menu)
                {
                    case '1':
                        Console.Write("Введите N:");
                        Int32.TryParse(Console.ReadLine(), out N);
                        a = new double[N];
                        for (int i = 0; i < N; i++)
                            a[i] = rnd.Next(0, 100);
                        res1.Clear();
                        break;

                    case '2':
                        Console.Write("Введите M:");
                        Int32.TryParse(Console.ReadLine(), out M);
                        break;

                    case '3':
                        Console.Write("Введите K:");
                        Int32.TryParse(Console.ReadLine(), out K);
                        res1.Clear();
                        break;

                    case '4':
                        Console.WriteLine("\n\n ...Выполняем последовательную обработку массива размером " + N);
                        res1.Clear();
                        int repeat = 0;

                        while (repeat < 25)
                        {
                            start = DateTime.Now;
                            posled(a, N, K);
                            stop = DateTime.Now;
                            ts = stop - start;
                            res1.Add(ts.TotalMilliseconds);
                            Console.WriteLine(repeat + ": " + ts.TotalMilliseconds);
                            repeat++;
                        }
                        break;

                    case '5':
                        if (M == 0 || M > N)
                            break;
                        res2.Clear();
                        Console.WriteLine("\n\n ...Выполняем параллельную обработку массива размером " + N + " \nЧисло потоков:" + M);
                        int part = N / M; //размеры кусков массива
                        int last = part + N % M; //последний, если не делится ровно
                        Thread[] threads = new Thread[M];
                        data = new Data[M];

                        /* Сначала инициализируем соответствующие потоки, только затем запускаем.
                            Просто хочется большей объективности в результатах, чтобы лишний код не добавлял времени
                         */

                        int times = 0;
                        while (times < 300) //прогоняем 300 раз
                        {
                            for (int i = 0; i < M; i++) // готовим потоки
                            {
                                threads[i] = new Thread(parallel);
                                threads[i].Name = i.ToString();

                                double[] b;
                                int end = 0;
                                if (i == (M - 1))   //следим достижение последнего потока
                                {
                                    end = N;
                                    b = new double[last];

                                }
                                else
                                {
                                    end = (i + 1) * part;
                                    b = new double[part];
                                }

                                data[i] = new Data(K, i * part, end, a, i);
                            }


                            start = DateTime.Now;
                            for (int i = 0; i < M; i++) //запускаем все потоки
                                threads[i].Start(data[i]);

                            for (int i = 0; i < M; i++)
                                threads[i].Join(); //ждем пока все выполнятся

                            stop = DateTime.Now;
                            ts = stop - start;
                            res2.Add(ts.TotalMilliseconds);
                            Console.WriteLine(times + ": " + ts.TotalMilliseconds);
                            times++;
                        }
                        break;
                    case '7':
                        if (res1.Count != 0)
                        {
                            Console.WriteLine("Результаты последовательной обработки: \n");
                            res1.ForEach(i => Console.WriteLine("{0}\t", i));
                            List<double> temp = res1.ToList();
                            temp.RemoveAt(0);
                            Console.WriteLine("\nСреднее время: " + temp.Sum() / temp.Count);
                        }

                        if (res2.Count != 0)
                        {
                            Console.WriteLine("Результаты параллельной обработки: \n");
                            res2.ForEach(i => Console.WriteLine("{0}\t", i));
                            List<double> temp = res2.ToList();
                            temp.RemoveAt(0);
                            Console.WriteLine("\nСреднее время: " + temp.Sum() / temp.Count);
                        }
                        break;

                    case '6':
                        if (M == 0 || M > N)
                            break;
                        res2.Clear();
                        Console.WriteLine("\n\n ...Выполняем параллельную обработку массива размером " + N + " \nЧисло потоков:" + M);
                        part = N / M; // сколько элементов обработает каждый поток
                        last = part + N % M; //последний поток, если не делится ровно
                        threads = new Thread[M];
                        data = new Data[M];

                        /* Сначала инициализируем соответствующие потоки, только затем запускаем.
                            Просто хочется большей объективности в результатах, чтобы лишний код не добавлял времени
                         */

                        times = 0;
                        while (times < 300) //прогоняем 300 раз
                        {
                            for (int i = 0; i < M; i++) // готовим потоки
                            {
                                threads[i] = new Thread(circlepar);
                                threads[i].Name = i.ToString();

                                data[i] = new Data(K, 0, 0, a, i);

                                double[] b;
                                
                                if (N%M != 0 && i < N%M)   //смотрим если пойдет на второй круг
                                {    
                                    b = new double[part+1];
                                    data[i].indexes = new int[part+1];
                                }
                                else
                                {
                                    b = new double[part];
                                    data[i].indexes = new int[part];
                                }

                                int el = 0; //выбираем только нужные элементы
                                for (int e = 0; e < N; e++)
                                    if ((e + M) % M == i)
                                    {
                                        data[i].indexes[el] = e;
                                        el++;
                                    }

                            }


                            start = DateTime.Now;
                            for (int i = 0; i < M; i++) //запускаем все потоки
                                threads[i].Start(data[i]);

                            for (int i = 0; i < M; i++)
                                threads[i].Join();

                            stop = DateTime.Now;
                            ts = stop - start;
                            res2.Add(ts.TotalMilliseconds);
                            Console.WriteLine(times + ": " + ts.TotalMilliseconds);
                            times++;
                        }
                        break;

                    default:
                        break;
                }

                Console.WriteLine("Нажмите любую кнопку для продолжения...");
                Console.ReadKey();
                Console.Clear();

            } while (menu != 'q');
        }

        static void posled(double[] a, int N, int K)
        {
            double[] b = new double[N];
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < i + 1; j++) // простой  for (int j = 0; j < K; j++)   j < i+1
                    b[i] = Math.Pow(a[i], 1.789);
            }
        }



        static void parallel(object obj) //double[] a, int K, int start, int stop)
        {
            Data data = (Data)obj;
            double[] b = new double[data.stop - data.start];
            int m = 0;
            for (int j = data.start; j < data.stop; j++)
            {
                for (int k = 0; k < j + 1; k++) // обычный - (int k = 0; k < K; k++)
                    b[m] = Math.Pow(data.a[j], 1.789);
                  //    Console.WriteLine(b[m] + " номер: " + Thread.CurrentThread.Name);
                m++;
            }
        }

        static void circlepar(object obj) //double[] a, int K, int start, int stop)
        {
            Data data = (Data)obj;
            double[] b = new double[data.indexes.Length];
            int m = 0;
            for (int i = 0; i < b.Length; i++)
            {
                for (int k = 0; k < data.indexes[i]+1; k++) // обычный - (int k = 0; k < K; k++)
                    b[m] = Math.Pow(data.a[data.indexes[i]], 1.789);
               //       Console.WriteLine(b[m] + " номер: " + Thread.CurrentThread.Name);
                m++;
            }
        }

    }
}
